# zoteroNetworks Package

## Getting Started
To install this package, make sure you have devtools installed in your R instance.
```R
install.packages('devtools')
library(devtools)
```

Then use devtools to install from gitlab.
```R
install_gitlab("camden-bock/zoteroNetworks")
```

This packages assumes that your zotero ID and API key will be stored in a .env file. See format below.
```
ZOTERO_USER=555555
ZOTERO_API=ASADIUFEWKJA230ADKJS2309
```

Make a new key for your Zotero Account at [Zotero.org](https://www.zotero.org/settings/keys/new).
Find the User ID for your Zoter Account at [Zotero.org](https://www.zotero.org/settings/keys).

## Requirements for Development
 - pandoc
 - devtools
 - roxygen

## Purpose
zoteroNetworks is inspired by [ZoteroR](https://github.com/giocomai/zoteroR), which is a library that focuses on uploading content of Data Frames to a Zotero Library.

My hope is to be able to retrieve data from Zotero (especially data from systematic literature reviews), to operate on as both data frames and as graphs.
I am partially interested in quick reporting of descriptive statistics (distribution amoung journals, counts, etc.).
This package will also be designed to support looking at relationships between collections (bins that literature is sorted into) and other attributes (e.g. Publication Title of a Journal).
These might be understood as a graph, where the nodes are these bins (collections, publication titles, publication years, etc.)
and the edges represent the number of items that share two attributes.

For example, an edge with a weight three between:
 - Journal of Research in Mathematics Education
 - Category: Virtual Reality

might represent that there are three articles from JRME about Virtual Reality.

If this package was successful, it would support individuals analysis of literature coded into Zotero Collections.
I hope that it will also allow for graphics to be rendered (e.g., Sankey Diagram). 
Those will use other tools (iGraph's Plot or NetworkD3) and will not be directly included in the package where possible.

## Methods for Retrieving Data

### Zotero User Query
 - *description* Queries Zotero Web API
 - **input** :  query string for query
 - **input** :  modifiers query modifiers for receiving limit and start
 - **input** :  user Zotero User ID
 - **input** :  apikey Zotero API Key
 - **return** jsonlite map of JSON query

### Get Collections
 - *description* Helper function to aid "Get All Collections" in curating a dataframe of all of the collections within Zotero
 - **input** :  user
 - **input** :  apikey
 - **input** :  start
 - **input** :  top,Default: FALSE
 - **return** dataframe of the top 100 collections within Zotero

### Get All Collections
 - *description* Curates a dataframe of all of the collections within Zotero
 - **input** :  user
 - **input** :  apikey
 - **input** :  start
 - **input** :  top,Default: FALSE
 - **return** dataframe of all the collections within Zotero

### Get Top Items
 - *description* Get top items is a helper function used by the function "Get All Items" in order to efficiently fetch the whole library. Zotero's top items include citation data but not links or attachments.
 - **input** :  user
 - **input** :  apikey
 - **input** :  start
 - **return** Dataframe containing the top Zotero items

### Get All Zotero Items
 - description Fetches all Zotero items
 - **input** : user
 - **input** : apikey
 - **return** Returns a dataframe containing all of the Zotero items

### List of Zotero Publications
 - *description* **return**s a list of Zotero publications
 - **input** :  items
 - **return** list of Zotero publications

### List Distinct Item Types
 - *description* **return**s a list of distinct Zotero item types
 - **input** :  items
 - **return** list of Zotero item types

## Methods for Constructing Graphs
### Collection Adjacenccy Matrix
 - *description* Construct a Adjacency Matrix
 - **input** items
 - **input** collections
 - **return** A n*n Matrix describing edge weights, 0 is not adjacent

### Collection and Publications Adjacenccy Matrix
 - *description* Construct a Adjacency Matrix with Publications and Collecctions
 - **input** items
 - **input** collections
 - **input** publications
 - **return** A n*n matrix describing edge weights, 0 is not adjacent

### Collection Contains Item
 - *description* Checks if a collectino contains an item
 - **input** itemCollections A list, containing the collectinos an item is in
 - **input** collectionKey the key for the collection of interest
 - **return** boolean

### Find Items in Collecction
 - *description* Selects items within a dataframe of Zotero Items
 - **input** items Zotero Items
 - **input** collectionKey Key for Collection of Interest
 - **return** a dataframe of Zotero items

### Count Items in Collection
 - *description* Counts Selected items within a dataframe of Zotero Items
 - **input** items Zotero Items
 - **input** collectionKey Key for Collection of Interest
 - **return** a count of Zotero items as an integer

### Find Items in Multiple Collections
 - *description* Finds items that share a common set of collections, specified with collection keys
 - **input** items Zotero Items
 - **input** collecitonKeys Collections
 - **return** dataframe of selected items

### Count Items in Multiple Collections
 - *description* Counts items that share a common set of collections, specified with collection keys
 - **input** items Zotero Items
 - **input** collecitonKeys Collections
 - **return** integer count

### Find Items in Publication
 - *description* Finds items that share a common publication title
 - **input** items Zotero Items (dataframe)
 - **input** publication Publication Title (string)
 - **return** a dataframe of Zotero Items

### Count Items in Publication
 - *description* Count items that share a common publication title
 - **input** items Zotero Items (dataframe)
 - **input** publication Publication Title (string)
 - **return** integer count

### Find Items of Type
 - *description* finds items of a document type,
 - **input** items a dataframe of Zotero Items
 - **input** itemType string, document type
 - **return** dataframe with matching Zotero Items

### Count Items of Type
 - *description* Counts items of a document type
 - **input** items dataframe of Zotero Items
 - **input** itemType string, document type
 - **return** integer count

### Find Publication in Collecction
 - *description* Finds Zotero Items that match a publication and Collection
 - **input** items dataframe of Zotero Items
 - **input** collectionKey string Collection
 - **input** publication string Publication Title
 - **return** dataframe of Zotero Items

### Count Publication in Collecction
 - *description* Counts Zotero Items that match a publication and Collection
 - **input** items dataframe of Zotero Items
 - **input** collectionKey string Collection
 - **input** publication string Publication Title
 - **return** integer count

### Find Item Type in Collection
 - *description* Finds items that match a document type and Collection
 - **input** items a dataframe of Zotero Items
 - **input** collectionKey string Collection
 - **input** type string document type
 - **return** a dataframe of Zotero Items

### Count Items in Collection
 - *description* Counts Selected items within a dataframe of Zotero Items
 - **input** items Zotero Items
 - **input** collectionKey Key for Collection of Interest
 - **return** a count of Zotero items as an integer

### Find Items in Multiple Collections
 - *description* Finds items that share a common set of collections, specified with collection keys
 - **input** items Zotero Items
 - **input** collecitonKeys Collections
 - **return** dataframe of selected items

### Count Items in Multiple Collections
 - *description* Counts items that share a common set of collections, specified with collection keys
 - **input** items Zotero Items
 - **input** collecitonKeys Collections
 - **return** integer count

### Find Items in Publication
 - *description* Finds items that share a common publication title
 - **input** items Zotero Items (dataframe)
 - **input** publication Publication Title (string)
 - **return** a dataframe of Zotero Items

### Count Items in Publication
 - *description* Count items that share a common publication title
 - **input** items Zotero Items (dataframe)
 - **input** publication Publication Title (string)
 - **return** integer count

### Find Items of Type
 - *description* finds items of a document type,
 - **input** items a dataframe of Zotero Items
 - **input** itemType string, document type
 - **return** dataframe with matching Zotero Items

### Count Items of Type
 - *description* Counts items of a document type
 - **input** items dataframe of Zotero Items
 - **input** itemType string, document type
 - **return** integer count

### Find Publication in Collecction
 - *description* Finds Zotero Items that match a publication and Collection
 - **input** items dataframe of Zotero Items
 - **input** collectionKey string Collection
 - **input** publication string Publication Title
 - **return** dataframe of Zotero Items

### Count Publication in Collecction
 - *description* Counts Zotero Items that match a publication and Collection
 - **input** items dataframe of Zotero Items
 - **input** collectionKey string Collection
 - **input** publication string Publication Title
 - **return** integer count

### Find Item Type in Collection
 - *description* Finds items that match a document type and Collection
 - **input** items a dataframe of Zotero Items
 - **input** collectionKey string Collection
 - **input** type string document type
 - **return** a dataframe of Zotero Items

### Count Item Type in Collection
 - *description* Counts items that match a document type and Collection
 - **input** items a dataframe of Zotero Items
 - **input** collectionKey string Collection
 - **input** type string document type
 - **return** integer count

## Methods for Manipulation as Graphs
### Zotero iGraph
 - *description* Creates an iGraph object from an adjacency matrix of Zotero Collections
 - **input** :  adjMatrix adjacency matrix with edge weights
 - **return** igraph object

### Zotero NetworkD3
 - *description* Converts a weighted graph representing a Zotero Library to a NetworkD3 object
 - **input** :  adjMatrix matrix with adjacencies marked as edge weights
 - **return** NetworkData ND3 object

### Chord Network
 - *description* Maps Zotero adjacency Matrix onto a ND3 Chord Network
 - **input** :  adjMatrix matrix with adjacencies marked as edge weights
 - **return** plots a ND3 dynamic graph
